/* 
 * File:   main.c
 * Author: viplime
 *
 * Created on March 25, 2014, 10:55 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "morpAnalyser.h"

#define PARSED_CORPORA_NAME "parsedCorpora.txt"

/*
 * argv[1] : trmorph.fst
 * argv[2] : Corpora
 * argv[3] : Test Corpora
 * argv[4] : N Gram
 */
int
main(int argc, char** argv)
{
    char tags[MAX_TAG_SIZE][MAX_TAG_LEN];
    char fileName[] = "XGram.txt";
    int tagCounts[MAX_TAG_SIZE] = {0};
    int size = 0, i = 0;

    if (argc == 5) {
        HelloMessage();
        //Analyse corpora
        printf("\nParsing corpora...");
        ParseWords(argv[1], argv[2], PARSED_CORPORA_NAME);
        printf("\nParsed corpora's been written to %s\n", PARSED_CORPORA_NAME);
        printf("\nCounting NGrams in corpora...");
        for (i = 0; i < atoi(argv[4]); ++i) {
            // Get unique tags from tag file
            ExtractTags(PARSED_CORPORA_NAME, i + 1, tags, &size);
            //Count ngrams
            CountNGram(PARSED_CORPORA_NAME, tags, size, tagCounts);
            // Print to file
            fileName[0] = (char) (((int) '0')+(i + 1)); //Set nGram No
            WriteNGramCounts(fileName, tags, tagCounts, size);
        }
        printf("\nNGram counts've been written to files named [N]Gram.txt\n");
        printf("\nCalculating probabilities of morphological structures in corpora...");
        // Calculate prob of morphological structure using bigrams
        ProbablityViaBigrams(argv[1], argv[3], PARSED_CORPORA_NAME);
        printf("\nProbabilities've been written to %s\n", "ProbOfMorphStrByBiGram.txt");
        //Evaluate models
        printf("\nEvaluation 1-2-3Gram Models...");
        EvaluateNGramModels(argv[1], argv[3], PARSED_CORPORA_NAME, 3);
        printf("\n\nEnd\n\n");
    }
    else {
        fprintf(stderr, "Bad command line arguments.\n");
        fprintf(stderr, "argv[1] : trmorph.fst\nargv[2] : Corpora\nargv[3] : Test Corpora\nargv[4] : NGram\n");
    }
    return (EXIT_SUCCESS);
}

