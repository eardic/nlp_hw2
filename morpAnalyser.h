/* 
 * File:   morpAnalyser.h
 * Author: viplime
 *
 * Created on 28 March 2014, 22:57
 */

#ifndef MORPANALYSER_H
#define	MORPANALYSER_H

#define MAX_WORD_LEN 200
#define MAX_LINE 500
#define MAX_TAG_LEN 500
#define MAX_TAG_SIZE 3000
#define MAX_TAG_PER_LINE (MAX_LINE/3)
#define MAX_NGRAM_LEN 500

// Analyses words morphologically and writes results in a text file
// FST and Corpora must be given properly as first and seconds parameters.
// Output File name should be given as last parameter.
int ParseWords(char* fst, char* corpora, char* outFileName);

void WriteNGramCounts(char* name, char tags[][MAX_TAG_LEN], int tagCounts[], int size);

void HelloMessage();

// Counts n gram for given tags group in given file.
int CountString(FILE* file, char* tagGroup);

void CountNGram(char* file, char tags[][MAX_TAG_LEN], int size, int counts[]);

void SplitTags(char* line, char tags[][MAX_TAG_LEN], int* size);

void CreateNGrams(const int N, char splitTags[][MAX_TAG_LEN], int splitTagsSize,
        char tags[][MAX_TAG_LEN], int* tagsSize, int unique);

void ProbablityViaBigrams(char* fst, char* test, char*parsedCorpora);

int CountAllTags(FILE* file);

int GetNGramCount(char nGram[], char uniqueNGrams[][MAX_TAG_LEN], int nGramCounts[], int size);

void EvaluateNGramModels(char* fst, char* test, char*parsedCorpora, int N);

int Contains(char arr[][MAX_TAG_LEN], int size, const char*el);

void ExtractTags(char *parsedCorpora, const int N, char tags[][MAX_TAG_LEN], int* tagsSize);




#endif	/* MORPANALYSER_H */

