/* 
 * File:   main.c
 * Author: viplime
 *
 * Created on March 25, 2014, 10:55 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <fomalib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "morpAnalyser.h"

void HelloMessage()
{
    printf("\n*****  GEBZE INSTITUTE OF TECHNOLOGY   *****");
    printf("\n*****  NATURAL LANGUGAGE PROCESSING    *****");
    printf("\n*****        HW 02 EMRE ARDIC          *****\n");
}

void WriteNGramCounts(char* name, char tags[][MAX_TAG_LEN], int tagCounts[], int size)
{
    FILE* filePtr = NULL;
    int i = 0;
    filePtr = fopen(name, "w");
    if (filePtr != NULL) {
        for (i = 0; i < size; ++i) {
            fprintf(filePtr, "%s\t%d\n", tags[i], tagCounts[i]);
        }
        fclose(filePtr);
    }
    else {
        fprintf(stderr, "\nCannot open file : %s", name);
    }
}

/*
 * Checks if given given string(el) is in given array of string
 * Return true if el is in arr, and false if it's not
 * arr  : array of string
 * size : size of arr
 * el   : the string which will be searched
 */
int Contains(char arr[][MAX_TAG_LEN], int size, const char* el)
{
    int j = 0;
    for (j = 0; j < size; ++j) {
        if (!strcmp(arr[j], el)) {
            return 1;
        }
    }
    return 0;
}

/*
 * Opens given file and counts given tags and prints them to console
 */
void CountNGram(char* file, char tags[][MAX_TAG_LEN], int size, int counts[])
{
    FILE* filePtr = NULL;
    int i = 0, count = 0;
    filePtr = fopen(file, "r");
    if (filePtr != NULL) {
        for (i = 0; i < size; ++i) {
            count = CountString(filePtr, tags[i]);
            //printf("%s\t%d\n", tags[i], count);
            counts[i] = count;
        }
        fclose(filePtr);
    }
    else {
        fprintf(stderr, "\nCannot open file : %s", file);
    }
}

/*
 * TagGroup is form of "<n><D_lAn><v>" or <N>
 * Given tags are searched in file and returns counts of it
 */
int CountString(FILE* file, char* tagGroup)
{
    int counter = 0;
    char line[MAX_LINE];
    char *temp = line;
    if (file != NULL && tagGroup != NULL && strlen(tagGroup) > 0) {
        rewind(file); //Reset line pointer of file
        while (!feof(file)) {
            fgets(line, MAX_LINE, file);
            //Count all occurences of given tagGroup in line
            temp = line;
            while ((temp = strstr(temp, tagGroup)) != NULL) {
                temp += strlen(tagGroup); //Skip the matched word
                ++counter; //Increment num of occurence of tagGroup in line
            }
        }
    }
    return counter;
}

int CountAllTags(FILE* file)
{
    char splitTags[MAX_TAG_PER_LINE][MAX_TAG_LEN]; // Tags in a line
    int splitTagsSize = 0;
    int counter = 0;
    char line[MAX_LINE] = {'\0'};
    if (file != NULL) {
        rewind(file); //Reset line pointer of file
        while (!feof(file)) {
            fgets(line, MAX_LINE, file);
            splitTagsSize = 0;
            SplitTags(line, splitTags, &splitTagsSize); //Extract tags to array
            counter += splitTagsSize; //Add num of tag to counter
        }
    }
    return counter;
}

void SplitTags(char* line, char tags[][MAX_TAG_LEN], int* size)
{
    char tag[MAX_TAG_LEN] = {'\0'}; // Single temp tag in string format
    int i = 0, j = 0;
    *size = 0;
    for (i = 0; i < strlen(line); ++i) {
        if (line[i] == '<') {
            // Get tag
            for (j = 0; line[j + i] != '>'; ++j) {
                tag[j] = line[i + j];
            }
            // Convert tag char array to a string
            tag[j] = '>';
            tag[j + 1] = '\0';
            // Save the tag in array of tags for the line
            strcpy(tags[*size], tag);
            ++(*size);
        }
    }
}

/*
 * 
 */
void CreateNGrams(const int N, char splitTags[][MAX_TAG_LEN], int splitTagsSize,
                  char tags[][MAX_TAG_LEN], int* tagsSize, int unique)
{
    int i = 0, j = 0;
    int nGramIter = 0;
    nGramIter = (splitTagsSize - (N - 1));
    char nGramTags[MAX_NGRAM_LEN] = {'\0'};
    for (i = 0; i < nGramIter; ++i) {
        // Check tags, and save them in arr of tags                
        for (j = 0, nGramTags[0] = '\0'; j < N; ++j) {
            strcat(nGramTags, splitTags[j + i]);
        }
        if (unique && !Contains(tags, *tagsSize, nGramTags)) {
            strcpy(tags[*tagsSize], nGramTags);
            *tagsSize = *tagsSize + 1;
        }
        else if (!unique) {
            strcpy(tags[*tagsSize], nGramTags);
            *tagsSize = *tagsSize + 1;
        }
    }
}

//Read train file and extract tags as an array of string

void ExtractTags(char* parsedCorpora, const int N, char tags[][MAX_TAG_LEN], int* tagsSize)
{
    FILE* parsedCorporaFile = NULL;
    char line[MAX_LINE]; // Read line
    char splitTags[MAX_TAG_PER_LINE][MAX_TAG_LEN]; // Tags in a line
    int lineC = 0, splitTagsSize = 0;
    *tagsSize = 0;
    parsedCorporaFile = fopen(parsedCorpora, "r");
    if (parsedCorporaFile != NULL) {
        for (lineC = 0; !feof(parsedCorporaFile); ++lineC) {
            fgets(line, MAX_LINE, parsedCorporaFile);
            SplitTags(line, splitTags, &splitTagsSize);
            CreateNGrams(N, splitTags, splitTagsSize, tags, tagsSize, 1);
        }
        fclose(parsedCorporaFile);
    }
    else {
        fprintf(stderr, "\nFile pointer is NULL!");
    }
}

int GetNGramCount(char nGram[], char uniqueNGrams[][MAX_TAG_LEN], int nGramCounts[], int size)
{
    int i = 0;
    for (i = 0; i < size; ++i) {
        if (!strcmp(uniqueNGrams[i], nGram)) {
            return nGramCounts[i];
        }
    }
    return 0;
}

void EvaluateNGramModels(char* fst, char* test, char*parsedCorpora, int N)
{
    FILE *corporaFile = NULL, *testFile = NULL;
    char *result = NULL;
    char readWord[MAX_WORD_LEN] = {'\0'};
    char splitTags[MAX_TAG_PER_LINE][MAX_TAG_LEN];
    int splitTagsSize = 0, n = 0, i = 0;
    char tags[MAX_TAG_SIZE][MAX_TAG_LEN];
    int tagSizes;
    int allTagCountInCorpora = 0; //For prob calculation
    int tagCount = 0, tagCountTemp = 0;
    double perplexities[N];

    // Arrays for NGram Counts
    char uniqueNGrams[N][MAX_TAG_SIZE][MAX_TAG_LEN];
    int nGramCounts[N][MAX_TAG_SIZE];
    int uniqueNGramsSize[N];

    struct fsm *net = NULL;
    struct apply_handle *ah = NULL;

    //Count ngrams
    for (n = 0; n < N; ++n) {
        ExtractTags(parsedCorpora, n + 1, uniqueNGrams[n], &uniqueNGramsSize[n]);
        CountNGram(parsedCorpora, uniqueNGrams[n], uniqueNGramsSize[n], nGramCounts[n]);
    }

    net = fsm_read_binary_file(fst);
    ah = apply_init(net);

    corporaFile = fopen(parsedCorpora, "r");
    testFile = fopen(test, "r");

    if (corporaFile != NULL && testFile != NULL) {

        allTagCountInCorpora = CountAllTags(corporaFile);
        memset(perplexities, 1, sizeof (double)*N);

        while (!feof(testFile)) {
            fscanf(testFile, "%s", readWord);
            // Get morph structure of words
            result = apply_up(ah, readWord);
            while (result != NULL) {
                splitTagsSize = 0;
                SplitTags(result, splitTags, &splitTagsSize);
                for (n = 0; n < N; ++n) {//1-2-3...N-Gram
                    tagSizes = 0;
                    CreateNGrams(n + 1, splitTags, splitTagsSize, tags, &tagSizes, 0);
                    for (i = 0; i < tagSizes; ++i) {
                        tagCount = GetNGramCount(tags[i], uniqueNGrams[n], nGramCounts[n], uniqueNGramsSize[n]);
                        if (tagCount > 0) {// Ignore zero count
                            if (n == 0) {//1 Gram
                                // Nth Root(1/P(Wn|W1^n-1))
                                perplexities[n] *= pow((double) tagCount / allTagCountInCorpora, -1.0 / allTagCountInCorpora);
                            }
                            else {//Other Grams
                                *strrchr(tags[i], '<') = '\0'; // Get Wn-1
                                // Get prob of Wn-1
                                tagCountTemp = GetNGramCount(tags[i], uniqueNGrams[n - 1], nGramCounts[n - 1], uniqueNGramsSize[n - 1]);
                                if (tagCountTemp > 0) {// Ignore if zero
                                    perplexities[n] *= pow((double) tagCount / tagCountTemp, -1.0 / allTagCountInCorpora);
                                }
                            }
                            //printf("\nProbs[%d] : %E", n + 1, probs[n]);
                        }
                    }
                }
                result = apply_up(ah, NULL);
                result = NULL;
            }
        }

        for (n = 0; n < N; ++n) {
            printf("\nPerplexity of %d Gram : %E", n + 1, perplexities[n]);
        }

        fclose(corporaFile);
        fclose(testFile);
        apply_clear(ah);
        fsm_destroy(net);
    }
    else {
        fprintf(stderr, "Failed to open or create given files.\n");
    }
}

void ProbablityViaBigrams(char* fst, char* test, char*parsedCorpora)
{
    struct fsm *net = NULL;
    struct apply_handle *ah = NULL;
    char *result = NULL;
    char readWord[MAX_WORD_LEN] = {'\0'};
    FILE *outFile = NULL, *corporaFile = NULL, *testFile = NULL;

    char splitTags[MAX_TAG_PER_LINE][MAX_TAG_LEN];
    int splitTagsSize = 0, i = 0;
    double prob = 0;
    char twoTags[MAX_TAG_LEN] = {'\0'};

    net = fsm_read_binary_file(fst);
    ah = apply_init(net);

    outFile = fopen("ProbOfMorphStrByBiGram.txt", "w");
    corporaFile = fopen(parsedCorpora, "r");
    testFile = fopen(test, "r");

    if (outFile != NULL && corporaFile != NULL && testFile != NULL) {

        while (!feof(testFile)) {
            fscanf(testFile, "%s", readWord);
            // Get morph structure of words
            result = apply_up(ah, readWord);
            if (result != NULL) {
                SplitTags(result, splitTags, &splitTagsSize);
                prob = 0;
                for (i = 0; i < splitTagsSize - 1; ++i) {
                    strcpy(twoTags, splitTags[i]);
                    strcat(twoTags, splitTags[i + 1]);
                    prob += log((double) CountString(corporaFile, twoTags) / CountString(corporaFile, splitTags[i]));
                    //printf("%s\t%s\t%lf\n", twoTags, splitTags[i], prob);
                }
                // Unigram cannot be calc by bigram
                if (splitTagsSize > 1) {
                    fprintf(outFile, "%s\t%E\n", strchr(result, '<'), exp(prob));
                }
            }/*
            else {
                fprintf(stderr, "\nFailed to parse : %s\n", readWord);
            }*/
        }
        fclose(outFile);
        fclose(corporaFile);
        fclose(testFile);
        apply_clear(ah);
        fsm_destroy(net);
    }
    else {
        fprintf(stderr, "Failed to open or create given files.\n");
    }

}

/*
 * fst : trmorph.fst
 * corpora : Corpora
 * output file : Analysis File
 */
int ParseWords(char* fst, char* corpora, char* outFileName)
{
    struct fsm *net = NULL;
    struct apply_handle *ah = NULL;
    char *result = NULL;
    char readWord[MAX_WORD_LEN];
    FILE *outFile = NULL, *inFile = NULL;

    net = fsm_read_binary_file(fst);
    ah = apply_init(net);

    outFile = fopen(outFileName, "w");
    inFile = fopen(corpora, "r");

    if (outFile != NULL && inFile != NULL) {

        while (!feof(inFile)) {
            fscanf(inFile, "%s", readWord);
            if (!strcmp(readWord, "<S>") || !strcmp(readWord, "</S>") ||
                (ispunct(readWord[0]) && strlen(readWord) < 2)) {
                fprintf(outFile, "%s\n", readWord);
            }
            else {
                result = apply_up(ah, readWord);
                while (result != NULL) {
                    fprintf(outFile, "%s\n", result);
                    result = apply_up(ah, NULL);
                }
            }
        }
        fclose(outFile);
        fclose(inFile);
        apply_clear(ah);
        fsm_destroy(net);
    }
    else {
        fprintf(stderr, "Failed to open or create given files.\n");
        return EXIT_FAILURE;
    }


    return (EXIT_SUCCESS);
}

